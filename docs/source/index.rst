.. status documentation master file, created by
   sphinx-quickstart on Thu Feb 13 11:59:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to status's documentation!
==================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   README.md
   CHANGELOG.md
   status