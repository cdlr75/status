""" Start a sample App for 30sec. """
import asyncio

from status import Server


class MyApp:
    """ A sample app. """

    def __init__(self):
        self.status_server = Server(host="127.0.0.1", port=8080)

    async def start(self):
        """ Start ou app. """
        print("start service...")
        self.status_server.add_route(self.status)
        await self.status_server.start()

    async def shutdown(self):
        """ Gracefull shutdown. """
        print("shutdown service...")
        await self.status_server.stop()

    async def status(self):
        """ Returns our service status.

        :returns: Misc info about our service.
        :rtype: dict
        """
        return {
            "name": "MyService",
            "status": "ok",
            "version": "v1"
        }


def main():
    loop = asyncio.get_event_loop()
    time_to_run = 30
    app = MyApp()
    loop.run_until_complete(app.start())
    print("App status available at http://127.0.0.1:8080/status")
    print("sleeping", time_to_run)
    loop.run_until_complete(asyncio.sleep(time_to_run))
    loop.run_until_complete(app.shutdown())


if __name__ == '__main__':
    main()
