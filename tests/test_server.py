from aiohttp import web
import aiohttp.test_utils
import asynctest

from status.server import Server


class TestServer(asynctest.TestCase):
    """ Tests Server classe. """

    async def setUp(self):
        self.host = "127.0.0.1"
        self.port = aiohttp.test_utils.unused_port()
        self.url = f"http://{self.host}:{self.port}"
        self.server = Server(self.host, self.port)
        self.handler = asynctest.CoroutineMock()
        self.handler.return_value = {}
        self.server.add_route(self.handler)
        await self.server.start()

    async def tearDown(self):
        await self.server.stop()

    async def test_server(self):
        # Given
        self.handler.return_value = {"status": "ok"}
        # When
        async with aiohttp.ClientSession() as session:
            async with session.get(f"{self.url}/status") as resp:
                data = await resp.json()
        # Then
        self.assertEqual(data, {"status": "ok"})

    async def test_stop_before_start(self):
        # Given
        server = Server(self.host, self.port)
        # When
        await server.stop()
